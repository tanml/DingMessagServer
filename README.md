# DingMessagServer
 **Winfrom，Webfrom发送钉钉消息** 
每天晚上自动发送企业销售报表 给钉钉指定用户

PC端效果图

![输入图片说明](https://gitee.com/uploads/images/2017/1017/104246_cee94f25_847386.png "屏幕截图.png")

手机端效果图

![输入图片说明](https://gitee.com/uploads/images/2017/1017/104415_e3dfa713_847386.png "屏幕截图222.png")

服务端后台

![输入图片说明](https://gitee.com/uploads/images/2017/1017/104452_5aefae06_847386.png "屏幕截图.png")

每个月服务端因为各种问题会挂掉一两次，开发了一个Web版的后台，手动补发

![输入图片说明](https://gitee.com/uploads/images/2017/1017/104619_45bff2e9_847386.png "屏幕截图.png")